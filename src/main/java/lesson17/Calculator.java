package lesson17;

@FunctionalInterface
public interface Calculator<T, U, R, Q, Y> {

    Y sum(T t, U u, R r, Q q);
}
