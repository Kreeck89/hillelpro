package lesson8.entity;

import java.util.Objects;

public class House {
    private String city;
    private String address;
    private String owner;
    private int number;
    private String color;

    public House() {
    }

    public House(final String city, final String address, final String owner, final int number, final String color) {
        this.city = city;
        this.address = address;
        this.owner = owner;
        this.number = number;
        this.color = color;
    }

    public String getCity() {
        return city;
    }

    public void setCity(final String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(final String address) {
        this.address = address;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(final String owner) {
        this.owner = owner;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(final int number) {
        this.number = number;
    }

    public String getColor() {
        return color;
    }

    public void setColor(final String color) {
        this.color = color;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final House house = (House) o;
        return number == house.number &&
               Objects.equals(city, house.city) &&
               Objects.equals(address, house.address) &&
               Objects.equals(owner, house.owner);
//               && Objects.equals(color, house.color);
    }

    @Override
    public int hashCode() {
        return Objects.hash(city, address, owner, number);
    }
}
