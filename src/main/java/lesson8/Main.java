package lesson8;

import lesson8.entity.Cat;
import lesson8.entity.House;

public class Main {
    public static void main(String[] args) {
//        final Cat cat = new Cat();
//        System.out.println(cat.toString());
//        String str = "example";
//        System.out.println(str.toString());
//
//        int value = 10;
//        value.toString(); // не является наследником класса Object

        // МЕТОДЫ КЛАССА OBJECT:

//        Object object = new Object();
//        Object object2 = new Object();
//        object2 = object;
//
//        final Class<?> aClass = object.getClass();
//        final int hashCode = object.hashCode();
//        System.out.println("hashCode of Object: " + hashCode);
//        System.out.println("hashCode of Object2: " + object2.hashCode());
//        System.out.println("object.equals(object2): " + object.equals(object2));
//        System.out.println("object == object2: " + (object == object2));
//        System.out.println("object.toString(): " + object.toString());
////        object2.clone(); // нужно реализовывать доп интерфейс для клона объектов
////        object.finalize(); // выпилили
////        object.notify(); // методы для работы с многопоточностью...
////        object.notifyAll();
////        object.wait();


        // РАБОТА СО СРАВНЕНИЕМ ОБЪЕКТОВ МЕЖДУ СОБОЙ:

//        final House myHouse = new House("Kyiv", "Malinovskogo", "Me", 13, "Black");
//        final House youHouse = new House("Lviv", "Azov", "You", 44, "Red");
//        final House youSameHouse = new House("Lviv", "Azov", "You", 44, "Green");
////        System.out.println(youSameHouse.getAddress());
////        youSameHouse = youHouse;
////        youHouse.setAddress("sdfsdfsdfs");
////        System.out.println(youSameHouse.getAddress());
//        System.out.println("myHouse.equals(youHouse): " + myHouse.equals(youHouse));
//        System.out.println("youHouse.equals(youSameHouse): " + youHouse.equals(youSameHouse));
//
//        System.out.println("myHouse.hashCode(): " + myHouse.hashCode());
//        System.out.println("youHouse.hashCode(): " + youHouse.hashCode());
//        System.out.println("youSameHouse.hashCode(): " + youSameHouse.hashCode());

        // РАЗНИЦА РАБОТЫ С ПРИМИТИВАМИ И ССЫЛОЧНЫМИ ОБЪЕКТАМИ:

        int val = 1000;
        System.out.println("val: " + val);
        changeSomePrimitive(val);
        System.out.println("val: " + val);
        final int changePrimitiveWithReturnResult = changePrimitiveWithReturn(val);
//        val = changePrimitiveWithReturn(val);
        System.out.println("changePrimitiveWithReturnResult: " + changePrimitiveWithReturnResult);

        final Cat cat = new Cat();
        cat.setName("Alex");
        System.out.println("cat.getName(): " + cat.getName());
        changeCatName(cat, "Rich");
        System.out.println("cat.getName(): " + cat.getName());
    }

    private static void changeSomePrimitive(int value) {
        value += 5;
        System.out.println("changeSomePrimitive value: " + value);
    }

    private static int changePrimitiveWithReturn(int value) {
        return value + 33;
    }

    private static void changeCatName(final Cat cat, final String newName) {
        cat.setName(newName);
    }
}
