package lesson3;

import lesson3.enity.Bengal;
import lesson3.enity.Cat;
import lesson3.enity.Persian;

public class CatService {

    public void catValidationAgeForVoice(Cat cat) {
        if (cat.getAge() > 5) {
            cat.voiceForCats();
        }
//        cat.move(); //не работает, так как не определена в классе Cat и родительском классе
        if (cat instanceof Bengal) {
            ((Bengal) cat).move();
        }
        if (cat instanceof Persian) {
            ((Persian) cat).move();
        }
    }

//    private static void persianValidationAgeForVoice(Persian persian) {
//        if (persian.getAge() > 5) {
//            persian.voiceForCats();
//        }
//    }
//
//    private static void persianValidationAgeForVoice(Bengal bengal) {
//        if (bengal.getAge() > 5) {
//            bengal.voiceForCats();
//        }
//    }
}
