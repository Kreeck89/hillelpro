package lesson3.enity;

public final class Persian extends Cat {

    public Persian() {
    }

    public Persian(final String name) {
        super(name);
    }

    public Persian(final String name, final int age) {
        super(name, age);
    }

    public void move() {
        System.out.println("Move for PERSIAN");
    }
}
