package lesson3.enity;

public class Cat extends Animal {
    private int age;

    public int getAge() {
        return age;
    }

    public void setAge(final int age) {
        this.age = age;
    }

    public Cat() {
    }

    public Cat(final String name) {
        super(name);
    }

    public Cat(final int age) {
        this.age = age;
    }

    public Cat(final String name, final int age) {
        super(name);
        this.age = age;
    }

    public void voiceForCats() {
        System.out.println("Cats voice");
    }
}
