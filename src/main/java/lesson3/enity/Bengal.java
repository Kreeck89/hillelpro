package lesson3.enity;

public class Bengal extends Cat {

    public Bengal() {
    }

    public Bengal(final String name, final int age) {
        super(name, age);
    }

    public void move() {
        System.out.println("Move for Bengals");
    }
}
