package lesson3.enity;

public class Dog extends Animal {
    private double weight;

    public double getWeight() {
        return weight;
    }

    public void setWeight(final double weight) {
        this.weight = weight;
    }

    public Dog() {
    }

    public Dog(final double weight) {
        this.weight = weight;
    }

    public Dog(final String name, final double weight) {
        super(name);
        this.weight = weight;
    }
}
