package lesson3.enity;

public class Animal {
    private String name;

    public Animal() {
    }

    public Animal(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public void voice() {
        System.out.println("Animal voice");
    }
}
