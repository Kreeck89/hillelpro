package lesson3;

import lesson3.enity.Animal;
import lesson3.enity.Bengal;
import lesson3.enity.Cat;
import lesson3.enity.Dog;
import lesson3.enity.Persian;

public class Main {
    public static void main(String[] args) {
//        final Animal animal = new Animal();
//        animal.voice();
//
//        final Cat cat = new Cat();
//        cat.setName("Boris");
//        cat.voice();
//        System.out.println("Cat name: " + cat.getName());
//
//        final Persian persian = new Persian();
//        System.out.println("Persian name is: " + persian.getName());
//
//        final Persian dad = new Persian("Dad", 3);
//        System.out.println("Cat name: " + dad.getName() + ", age: " + dad.getAge());

        final Persian pers = new Persian("pers", 6);
        final Bengal ben = new Bengal("Ben", 8);
//        persianValidationAgeForVoice(pers);
//        persianValidationAgeForVoice(ben);

        final CatService catService = new CatService();
        catService.catValidationAgeForVoice(pers);
        catService.catValidationAgeForVoice(ben);

        final Dog dog = new Dog("bobby", 12.0);
        final Animal animal = new Animal("some animal");
//        catService.catValidationAgeForVoice(dog); // нельзя передать Dog вместо Cat
//        catService.catValidationAgeForVoice(animal); // нельзя передать Animal вместо Cat
        final AnimalService animalService = new AnimalService();
        animalService.printName(animal);
        animalService.printName(dog);
        animalService.printName(ben);

    }
}
