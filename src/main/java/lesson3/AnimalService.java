package lesson3;

import lesson3.enity.Animal;

public class AnimalService {

    public void printName(Animal animal) {
        System.out.println("Animal's name is: " + animal.getName().toUpperCase());
    }
}
