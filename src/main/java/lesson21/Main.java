package lesson21;

import lesson21.thread.CustomCallable;
import lesson21.thread.CustomRunnable;
import lesson21.thread.CustomThread;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class Main {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
//        System.out.println(Thread.currentThread().getName());
//
//        //СОЗДАНИЕ ПОТОКА 1:
//        final CustomThread customThread = new CustomThread();
////        customThread.run(); // Поток не создается! Просто вызов метода!
//        customThread.start();
//        new CustomThread().start();
//
//        //СОЗДАНИЕ ПОТОКА 2:
//        final CustomRunnable customRunnable = new CustomRunnable();
//        final Thread runnableThread = new Thread(customRunnable);
//        runnableThread.start();
//
//        Thread.sleep(1000);
//        System.out.println("CustomRunnable is alive: " + runnableThread.isAlive());
//
//        //СОЗДАНИЕ ПОТОКА 3:
//        final CustomCallable customCallable = new CustomCallable();
//        final FutureTask<String> stringFutureTask = new FutureTask<>(customCallable);
//        final Thread thread = new Thread(stringFutureTask);
//        thread.start();
//        final String result = stringFutureTask.get();
//        System.out.println("CustomCallable result is: " + result);
//
//        //СОЗДАНИЕ ПОТОКА 4 (без дополнительного класса):
//        final Thread threadWithoutCreatingNewClass = new Thread(() -> {
//            for (int i = 0; i < 100; i++) {
//                System.out.println("THREAD WITHOUT CREATING NEW CLASS. index = " + i +
//                                   ". Thread name = " + Thread.currentThread().getName());
//            }
//        });
//        threadWithoutCreatingNewClass.start();
////        threadWithoutCreatingNewClass.start(); // ПОТОКИ ОДНОРАЗОВЫЕ!!!!
//
//        // ПРИМЕР РАБОТЫ В main ПОТОКЕ:
////        for (int i = 0; i < 100; i++) {
////            System.out.println("THREAD WITHOUT CREATING NEW CLASS. index = " + i +
////                               ". Thread name = " + Thread.currentThread().getName());
////        }
//        System.out.println("THE END!!! Thread name = " + Thread.currentThread().getName());
//        System.out.println("THE END!!! Thread name = " + Thread.currentThread().getName());
//        System.out.println("THE END!!! Thread name = " + Thread.currentThread().getName());

//        runFewThreads();

//        final Thread thread = new Thread(() -> {
//            int i = 0;
//            while (true) {
//                System.out.println("variable: " + i++);
//            }
//        });
//        thread.start();
//
//        if (!thread.isInterrupted()) {
//            thread.interrupt();
//        }

//        runFewThreadsWithJoin();
        final CustomThread customThread = new CustomThread();
        customThread.start();
        customThread.join();
        System.out.println("End of Lesson 21!!!!");
    }

    private static void runFewThreads() {
        for (int i = 0; i < 10; i++) {
            int finalI = i;
            new Thread(() -> {
                Thread.currentThread().setName("ALEX " + finalI);
                Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
                System.out.println("runFewThreads: " + Thread.currentThread().getName());
            }).start();
        }
    }

    private static void runFewThreadsWithJoin() throws InterruptedException {
        for (int i = 0; i < 10; i++) {
            final Thread thread = new Thread(() -> {
                for (int j = 0; j < 100; j++) {
                    System.out.println(Thread.currentThread().getName() + ": " + j);
                }
            });
            thread.start();
            thread.join();
        }
    }
}
