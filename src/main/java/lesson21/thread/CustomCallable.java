package lesson21.thread;

import java.util.concurrent.Callable;

public class CustomCallable implements Callable<String> {

    @Override
    public String call() {
        return "DEFAULT STRING EXAMPLE";
    }
}
