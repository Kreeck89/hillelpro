package lesson21.thread;

public class CustomRunnable implements Runnable {

    @Override
    public void run() {
        System.out.println("CustomRunnable with thread: " + Thread.currentThread().getName());
        System.out.println("Thread is alive: " + Thread.currentThread().isAlive());
    }
}
