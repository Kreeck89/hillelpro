package lesson31.dao;

import lesson31.entity.House;
import lesson31.util.HibernateConfiguration;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class HouseDao {

    public void save(House house) {
        final SessionFactory sessionFactory = HibernateConfiguration.getSessionFactory();
        final Session session = sessionFactory.openSession();
        final Transaction transaction = session.beginTransaction();

        session.save(house);

        house.setAddress("CHANGED address");

        transaction.commit();
        session.close();
    }

    public House getById(Integer id) {
        final SessionFactory sessionFactory = HibernateConfiguration.getSessionFactory();
        final Session session = sessionFactory.openSession();
        final Transaction transaction = session.beginTransaction();

        House house = session.get(House.class, id);
        house = session.get(House.class, id);
        house = session.get(House.class, id);
        house = session.get(House.class, id);

        house.setAddress("CHANGED address");

        transaction.commit();
        session.close();

        return house;
    }
}
