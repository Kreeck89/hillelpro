package lesson31.service;

import lesson31.dao.HouseDao;
import lesson31.entity.House;

public class HouseService {
    private HouseDao houseDao = new HouseDao();

    public void save(House house) {
        houseDao.save(house);
    }

    public House getById(Integer id) {
        return houseDao.getById(id);
    }
}
