package lesson14;

import lesson14.entity.Admin;
import lesson14.entity.Client;
import lesson14.entity.User;

import java.util.ArrayList;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) {
//        //В этой коллекции класть ТОЛЬКО переменные типа String
//        final ArrayList stringList = new ArrayList();
//        stringList.add("String1");
//        stringList.add("String2");
//        stringList.add("String3");
//        stringList.add("String4");
//        stringList.add(10);
//        stringList.add(new Client());
//        checkListWithAnyEntities(stringList);

//        ArrayList<String> strings = new ArrayList<String>(); // Коллекция только для строк
//        ArrayList<String> strings = new ArrayList<>(); // Коллекция только для строк
//        strings.add("10");
//        strings.add("String");
//        strings.add(new Client().toString());
//
////        ArrayList<Client> clients = new ArrayList<Client>(); // Коллекция только для Клиентов
//        ArrayList<Client> clients = new ArrayList<>(); // Коллекция только для Клиентов
//        clients.add(new Client());
////        clients.add("Str");
//
////        ArrayList<Integer> ints = new ArrayList<Integer>();
//        ArrayList<Integer> ints = new ArrayList<>();
//        final Integer integer = Integer.valueOf("10");
//        final Double aDouble = Double.valueOf("10.2");
//        int val = 10;
//        ints.add(10);
//        ints.add(20);
//        ints.add(30);
//        ints.add(val);
//
////        ArrayList<Long> longs = new ArrayList<Long>();
//        ArrayList<Long> longs = new ArrayList<>();
//        int valInt = 10;
//        long valLong = 10L;
//        longs.add((long) valInt);
//        ints.add((int) valLong);
//
////        HashSet<String> stringHashSet = new HashSet<String>();
//        HashSet<String> stringHashSet = new HashSet<>();
//        stringHashSet.add("some string");
//
////        HashMap<String, Client> stringClientHashMap = new HashMap<String, Client>();
//        HashMap<String, Client> stringClientHashMap = new HashMap<>();
//        stringClientHashMap.put("Alex", new Client());
//        stringClientHashMap.put("Bob", new Client());
//        stringClientHashMap.put("ADMIN", new Admin());
//
////        HashMap<String, HashMap<String, ArrayList<Client>>> bigHashMap = new HashMap<String, HashMap<String, ArrayList<Client>>>();
//        HashMap<String, HashMap<String, ArrayList<Client>>> bigHashMap = new HashMap<>();
//
//        ArrayList<ArrayList<String>> listWithLists = new ArrayList<>();
//        final ArrayList<String> list = new ArrayList<>();
//        list.add("first");
//        list.add("second");
//        list.add("third");
//        listWithLists.add(list);
//
//        ArrayList<Number> numbers = new ArrayList<>();
//        numbers.add(valInt);
//        numbers.add(valLong);

        //Wildcards:
        final ArrayList<Client> clients = new ArrayList<>();
        clients.add(new Client());
        clients.add(new Client());
        clients.add(new Admin());
        final ArrayList<Admin> admins = new ArrayList<>();
        admins.add(new Admin());
        //        admins.add(new Client()); // НЕ МОЖЕМ ДОБАВИТЬ КЛАСС ВЫШЕ НАСЛЕДНИКА!
        final ArrayList<User> users = new ArrayList<>();
        users.add(new User());
        users.add(new Admin());
//        users.add(new Client()); // НЕ МОЖЕМ ДОБАВИТЬ КЛАСС ВЫШЕ НАСЛЕДНИКА!
//        users.add(new Object()); // НЕ МОЖЕМ ДОБАВИТЬ КЛАСС ВЫШЕ НАСЛЕДНИКА!
        checkListWithExtendsWildcard(clients);
        checkListWithExtendsWildcard(admins);

        checkListWithSuperWildcard(clients);
        checkListWithSuperWildcard(users);
//        checkListWithSuperWildcard(admins); // Amin - ниже чем User в иерархии наследования, потому НЕДОПУСТИМ!

        final HashMap<String, HashMap<String, String>> studentAnswers = new HashMap<>();
        final HashMap<String, String> alexAnswers = new HashMap<>();
        alexAnswers.put("Q1", "A1");
        alexAnswers.put("Q2", "A2");
        alexAnswers.put("Q3", "A3");
        studentAnswers.put("Alex", alexAnswers);
    }

    private static void checkListWithExtendsWildcard(ArrayList<? extends Client> clients) {
        for (Client client : clients) {
            client.say();
            client.setName("changed name");
            if (client instanceof Admin) {
                //do something...
            }
        }
//        clients.add(new Client()); // НЕ МОЖЕМ ДОБАВИТЬ ЧТО-ТО В КОЛЛЕКЦИЮ!!!
//        clients.add(new Admin()); // НЕ МОЖЕМ ДОБАВИТЬ ЧТО-ТО В КОЛЛЕКЦИЮ!!!
    }

    private static void checkListWithSuperWildcard(ArrayList<? super User> clients) {
        clients.add(new User());
        clients.add(new Admin());
//        clients.add(new Client()); // НЕ МОЖЕМ ДОБАВИТЬ, ТАК КАК НЕ НАСЛЕДНИК ОТ User!
    }

    private static void checkListWithAnyEntities(ArrayList list) {
        for (Object variable : list) {
//            System.out.println((String) variable); // Будет ClassCastException если тип переменной не корректный!
            if (variable instanceof String) {
                System.out.println(variable);
            }
            if (variable instanceof Integer) {
                System.out.println(variable);
            }
            if (variable instanceof Client) {
                System.out.println(variable);
            }
        }
    }
}
