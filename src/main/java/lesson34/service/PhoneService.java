package lesson34.service;

import org.apache.log4j.Logger;

public class PhoneService {
    private final Logger logger = Logger.getLogger(PhoneService.class);
    public void checkPhoneNumber(final Integer number) {
        logger.debug(String.format("checkPhoneNumber. Phone number for validation: %d", number));
        //some logic...
    }
}
