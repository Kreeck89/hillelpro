package lesson34;

import lesson34.controller.PhoneController;
import lesson34.entity.Phone;

import java.util.logging.Logger;

public class Main {
    public static void main(String[] args) {
//        System.err.println("Error");
        final PhoneController phoneController = new PhoneController();
        final Phone mobile = new Phone(777, "home", null);
        phoneController.checkPhoneNumber(mobile);
    }
}
