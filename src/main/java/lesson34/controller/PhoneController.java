package lesson34.controller;

import lesson34.entity.Phone;
import lesson34.service.PhoneService;
import lesson34.util.LoggerUtil;
import org.apache.log4j.Logger;

import java.util.logging.Level;
//import java.util.logging.Logger;

public class PhoneController {
    private final PhoneService phoneService = new PhoneService();
//    private final Logger logger = LoggerUtil.getLogger(PhoneController.class);
    private final Logger logger = Logger.getLogger(PhoneController.class);

    public void checkPhoneNumber(final Phone phone) {
//        logger.log(Level.INFO, phone.toString());
        logger.debug(String.format("checkPhoneNumber. Checking number = '%d' for id = '%d'", phone.getNumber(), phone.getId()));
        if (phone.getNumber() == null) {
            logger.error(String.format("checkPhoneNumber. Phone number is null for phone with id = {%d}", phone.getId()));
        } else {
            phoneService.checkPhoneNumber(phone.getNumber());
        }
    }
}
