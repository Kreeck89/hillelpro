package lesson29;

import lesson29.model.Customer;
import lesson29.service.CustomerService;

public class Main {
    public static void main(String[] args) {
        final CustomerService customerService = new CustomerService();
//        final Customer customer = new Customer();
////        customer.setId(9);
//        customer.setName("Vlad");
//        customer.setAge(34);
//        customer.setSurname("Save");
//        customer.setEmail("save_Vlad55@mail.ua");
//        customer.setPassword("123");
//        customerService.save(customer);
        final Customer customerById = customerService.getById(1);
        System.out.println(customerById.toString());
    }
}
