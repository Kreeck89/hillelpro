package lesson33.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/welcome")
public class WelcomeServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // ВЫНЕСТИ В Service И РАЗБИТЬ НА СЛОИ!!!!
//        final String email = String.valueOf(req.getSession().getAttribute("email"));
//        if (email != null) {
//            //some logic...
//        }
        req.getRequestDispatcher("welcome.jsp").forward(req, resp);
    }
}
