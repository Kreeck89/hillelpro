package lesson13;

import lesson2.User;
import lesson3.enity.Cat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;
import java.util.TreeSet;

public class Main {
    public static void main(String[] args) {

        // LIST:
        ArrayList arrayList = new ArrayList(1000);
        final LinkedList linkedList = new LinkedList();
        System.out.println("arrayList.size(): " + arrayList.size());

        arrayList.add("First");
        arrayList.add(12);
        System.out.println("arrayList.size(): " + arrayList.size());
        System.out.println("arrayList: " + arrayList);

        arrayList.add(0, "Zero");
        System.out.println("arrayList: " + arrayList);

        arrayList.set(2, "Second");
        System.out.println("arrayList: " + arrayList);

        for (int i = 0; i < arrayList.size(); i++) {
            final Object element = arrayList.get(i);
            System.out.print(element.toString() + " * ");
        }

        System.out.println();
        System.out.println("arrayList.get(2).toString(): " + arrayList.get(2).toString());

//        arrayList = null;
        if (arrayList != null && !arrayList.isEmpty()) {
            System.out.println("arrayList is not empty!");
        }

//        for (int i = 0; i < arrayList.size(); i++) {
//            if (arrayList.get(i).equals("someString")) {
//                //logic...
//            }
//        }
        final String someString = "someString";
        if (arrayList.contains(someString)) {
            //logic...
            System.out.println("arrayList contains " + someString);
        }

        System.out.println("arrayList.size(): " + arrayList.size());
        arrayList.remove(someString);
        System.out.println("arrayList.size(): " + arrayList.size());

        arrayList.add("Zero");
        arrayList.remove("Zero");
        System.out.println("arrayList.size(): " + arrayList.size());
        System.out.println(arrayList);

        arrayList.clear();
        System.out.println("arrayList.size(): " + arrayList.size());


        //SET:
        final TreeSet treeSet = new TreeSet();
        final HashSet hashSet = new HashSet();
        hashSet.add(12345);
        hashSet.add(12345);
        System.out.println("hashSet.contains(12): " + hashSet.contains(12));
        System.out.println("hashSet.size(): " + hashSet.size());
        final Iterator iterator = hashSet.iterator();
        while (iterator.hasNext()) {
            System.out.println("iterator.next(): " + iterator.next());
        }

        final ArrayList arrayListFroHashSet = new ArrayList(hashSet);
        final HashSet hashSetFroArrayList = new HashSet(arrayListFroHashSet);

        arrayListFroHashSet.get(0);
        System.out.println("arrayListFroHashSet: " + arrayListFroHashSet);
        hashSet.remove("some");
        hashSet.clear();

        //MAP:
        final HashMap hashMap = new HashMap();
//        hashMap.put(new Cat(), new User()); // Это страшно!
//        hashMap.put(1, "first");
        final Human alex = new Human("Alex", 10);
        hashMap.put("Alex", alex);
        hashMap.put("Bob", new Human("Bob", 20));
        hashMap.put(null, new Human("John", 30));
        hashMap.put("John", null);
        hashMap.put("John", new Human()); // происходит перезапись value в map

        System.out.println("hashMap.containsKey(\"Alex\"): " + hashMap.containsKey("Alex"));
        System.out.println("hashMap.containsValue(\"Alex\"): " + hashMap.containsValue(alex));

        System.out.println("hashMap.size(): " + hashMap.size());

        final Set set = hashMap.keySet();
        final Collection collection = hashMap.values();

        final Object david = hashMap.get("Alex");
        System.out.println(david.toString());
        hashMap.remove("Alex");
//        System.out.println(hashMap.get("Alex").toString()); // NPE так как Alex`a уже нет!

        final ArrayList tests = new ArrayList();
        tests.add("one");
        tests.add("two");
        tests.add("three");
        hashMap.put("Alex", tests);

        final ArrayList listWithLists = new ArrayList();
        listWithLists.add(tests);
    }
}
