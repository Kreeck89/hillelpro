package lesson13;

public class Human {
    private String name;
    private int age;

    public Human() {
    }

    public Human(final String name, final int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(final int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Human{" +
               "name='" + name + '\'' +
               ", age=" + age +
               '}';
    }
}
