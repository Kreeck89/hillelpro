package lesson10;

import java.io.IOException;
import java.net.BindException;
import java.nio.file.AccessDeniedException;
import java.nio.file.FileSystemException;
import java.util.Random;

public class ExceptionService {

    public void threwCheckedException() throws IOException {
        throw new IOException();
    }

    public void throwUncheckedException() {
        throw new RuntimeException();
//        throw new OutOfMemoryError();
    }

    public void throwTwoCheckedExceptions() throws FileSystemException, BindException {
        final int random = new Random().nextInt();
        if (random < 0) {
            throw new AccessDeniedException("file");
        } else {
            throw new BindException("msg");
        }
    }
}
