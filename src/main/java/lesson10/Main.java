package lesson10;

import java.io.IOException;
import java.net.BindException;
import java.nio.file.FileSystemException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws FileSystemException, BindException {
       String string = "example";
       checkString(string);
//       checkString(null);

        final ExceptionService exceptionService = new ExceptionService();
//        exceptionService.throwUncheckedException();
//        exceptionService.threwCheckedException();
//        exceptionService.throwTwoCheckedExceptions();

//        throw new MyCustomException();

//        try {
//            exceptionService.threwCheckedException();
//        } catch (IOException e) {
//            System.out.println("Catch IOException");
//            exceptionService.throwUncheckedException();
//        } finally {
//            System.out.println("finally step in try-catch");
//        }

//        Scanner scanner = new Scanner(System.in);
//        try {
//            System.out.println("Enter you name:");
//            final String name = scanner.next();
//            System.out.println("Your name is: " + name);
//            exceptionService.threwCheckedException();
//        } catch (IOException exception) {
//            System.out.println(exception.getMessage());
//            exception.printStackTrace();
//            scanner = null;
//        } finally {
//            System.out.println("!!!!");
//            try {
//                scanner.close();
//            } catch (NullPointerException e) {
//                System.out.println("scanner is null");
//            }
//            System.out.println("MESSAGE AFTER EXCEPTION IN FINALLY MODE");
//        }

        try (final Scanner scanner = new Scanner(System.in)) {
            System.out.println("Enter you age:");
            final int age = scanner.nextInt();
            System.out.println("Age is: " + age);
            exceptionService.throwTwoCheckedExceptions();
        } catch (FileSystemException e) {
            //logic 1
            System.out.println("GOT FileSystemException" + e.getMessage());
        } catch (BindException e) {
            //logic 2
            System.out.println("GOT BindException" + e.getMessage());
        } finally {
            System.out.println("try with resources....");
        }

        try {
            exceptionService.throwTwoCheckedExceptions();
        } catch (FileSystemException | BindException e) {
            //same logic!
            System.out.println("GOT some exception " + e.getMessage());
//            throw new MyCustomException();
            throw e;
        }
    }

    private static void checkString(String string) {
//        if (string == null) {
//            System.out.println("String is null");
//        }
        System.out.println("String length: " + string.length());
    }
}
