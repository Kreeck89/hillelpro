package lesson23;

import lesson23.lock.LockExample;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicIntegerArray;

public class Main {
    private static volatile int counter = 0;
    private static AtomicInteger atomicCounter = new AtomicInteger(0);

    public static void main(String[] args) throws InterruptedException {
//        final Thread thread = new Thread(() -> System.out.println("Hello"));
//        final Thread runnableThread = new Thread(new RunnableThread()); // аналог пример строчкой выше с дополнительным классом

        //Разница работы volatile и atomic переменных:
//        for (int i = 0; i < 10; i++) {
//            final Thread thread = new Thread(() -> {
//                for (int j = 0; j < 1000; j++) {
//                    atomicCounter.incrementAndGet();
////                    counter++;
//                }
//            });
//            thread.start();
//        }
//
//        Thread.sleep(2000);
////        System.out.println("Counter result: " + counter);
//        System.out.println("Counter result: " + atomicCounter.get());

//        volatileExample();

        //Atomic переменный для работы с атомарными операциями, такими как инкременет/декремент:
//        final AtomicInteger atomicInteger = new AtomicInteger(0);
//        atomicInteger.incrementAndGet(); // i++
//        atomicInteger.incrementAndGet(); // i++
//        atomicInteger.incrementAndGet(); // i++
//        System.out.println("atomicInteger.get(): " + atomicInteger.get());

        //Коллекции для многопоточности:
        final List<String> synchronizedStringList = Collections.synchronizedList(new ArrayList<>());
        final Set<String> synchronizedStringSet = Collections.synchronizedSet(new HashSet<>());
        final Map<String, String> synchronizedStringMap = Collections.synchronizedMap(new HashMap<>());

        final ConcurrentHashMap<String, String> concurrentHashMap = new ConcurrentHashMap<>();
        final CopyOnWriteArrayList<String> copyOnWriteArrayList = new CopyOnWriteArrayList<>();
        final Set<String> keySet = ConcurrentHashMap.newKeySet();

//        lockExample();

        //Бассейн потоков:
//        final ExecutorService executorService = Executors.newFixedThreadPool(10);
//        final ExecutorService executorService = Executors.newSingleThreadExecutor();
        final ExecutorService executorService = Executors.newCachedThreadPool();
        executorService.execute(() -> System.out.println("newCachedThreadPool"));
        executorService.shutdownNow();
    }

    /**
     * Пример работы нескольких потоков с volatile переменной
     */
    private static void volatileExample() {
        final Thread thread1 = new Thread(() -> {
            while (counter < 10) {
                System.out.println("Thread 1. counter: " + ++counter);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        });
        thread1.start();

        final Thread thread2 = new Thread(() -> {
            int cachedCounter = counter;
            while (counter < 10) {
//                System.out.println("waiting...");
                if (cachedCounter != counter) {
                    System.out.println("Thread 2. counter: " + counter);
                    cachedCounter = counter;
                }
            }
        });
        thread2.start();
    }

    /**
     * Пример использования Блокировок при работе с несколькими методами
     */
    private static void lockExample() {
        final LockExample lockExample = new LockExample();

        final Thread thread1 = new Thread(new Runnable() {
//            private LockExample lockExample = new LockExample();

            @Override
            public void run() {
                lockExample.print();
                lockExample.write();
            }
        });

        final Thread thread2 = new Thread(new Runnable() {
//            private LockExample lockExample = new LockExample();

            @Override
            public void run() {
                lockExample.print();
                lockExample.write();
            }
        });
        thread1.start();
        thread2.start();
    }
}
