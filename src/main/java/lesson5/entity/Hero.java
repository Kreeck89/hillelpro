package lesson5.entity;

public abstract class Hero {
    private String name;
    private double health;
    private int power;
    private int speed;

    protected Hero() {
    }

    protected Hero(final String name) {
        this.name = name;
    }

    protected Hero(final String name, final double health, final int power, final int speed) {
        this.name = name;
        this.health = health;
        this.power = power;
        this.speed = speed;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public double getHealth() {
        return health;
    }

    public void setHealth(final double health) {
        this.health = health;
    }

    public int getPower() {
        return power;
    }

    public void setPower(final int power) {
        this.power = power;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(final int speed) {
        this.speed = speed;
    }

    @Override
    public String toString() {
        return "Hero{" +
               "name='" + name + '\'' +
               ", health=" + health +
               ", power=" + power +
               ", speed=" + speed +
               '}';
    }
}
