package lesson5;

import lesson5.entity.Hero;
import lesson5.service.Action;
import lesson5.service.BigArenaService;
import lesson5.service.SmallArenaService;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        final Game game = new Game();
        final Hero first = game.chooseHero();
        final Hero second = game.chooseHero();
        game.setDataToHero(first);
        game.setDataToHero(second);
        System.out.println("Do you want to add third hero for fight?\n 1 - yes\n 2 - no");
        final Scanner scanner = new Scanner(System.in);
        final int result = scanner.nextInt();
        if (result == 1) {
            final Hero third = game.chooseHero();
            game.setDataToHero(third);
            final BigArenaService bigArenaService = new BigArenaService();
            bigArenaService.fight(first, second, third);
        } else {
            final SmallArenaService smallArenaService = new SmallArenaService();
            smallArenaService.fight(first, second);
        }
    }
}
