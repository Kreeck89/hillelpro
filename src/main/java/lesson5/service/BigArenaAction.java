package lesson5.service;

import lesson5.entity.Hero;

public interface BigArenaAction {
    public void fight(Hero first, Hero second, Hero third) throws InterruptedException;
}
