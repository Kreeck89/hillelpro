package lesson5.service;

import lesson5.entity.Hero;
import lesson5.entity.SuperPower;

public abstract class Action implements ArenaAction {

//public abstract void wrongLogic();

    @Override
    public void fight(Hero first, Hero second) throws InterruptedException {
        System.out.println("fight between:");
        System.out.println(first.toString());
        System.out.println(second.toString());

        Thread.sleep(5000);

        if (first instanceof SuperPower) {
            System.out.println(first.getName() + " is winner! He has SuperPower");
            return;
        }

        if (second instanceof SuperPower) {
            System.out.println(second.getName() + " is winner! He has SuperPower");
            return;
        }

        if (first.getHealth() > second.getHealth() && first.getSpeed() > second.getSpeed()) {
            System.out.println(first.getName() + " is winner!");
        } else if (first.getPower() < second.getPower()) {
            System.out.println(second.getName() + " is winner!");
        } else {
            System.out.println("Both was died, sorry!");
        }
    }
}
