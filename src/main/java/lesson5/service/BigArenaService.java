package lesson5.service;

import lesson5.entity.Hero;

public class BigArenaService extends Action implements BigArenaAction {

    @Override
    public void fight(Hero first, Hero second, Hero third) throws InterruptedException {
        System.out.println("fight between:");
        System.out.println(first.toString());
        System.out.println(second.toString());
        System.out.println(third.toString());
        Thread.sleep(5000);

        if (first.getPower() > second.getPower() && first.getPower() > third.getPower()) {
            System.out.println(first.getName() + " is winner!");
        } else if (second.getPower() > first.getPower() && second.getPower() > third.getPower()) {
            System.out.println(second.getName() + " is winner!");
        } else if (third.getPower() > first.getPower() && third.getPower() > second.getPower()) {
            System.out.println(third.getName() + " is winner!");
        } else {
            System.out.println("All heroes was died (((");
        }
    }
}
