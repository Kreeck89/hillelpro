package lesson5.service;

import lesson5.entity.Hero;

public interface ArenaAction {
    public void fight(Hero first, Hero second) throws InterruptedException;
}
