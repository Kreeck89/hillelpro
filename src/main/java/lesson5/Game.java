package lesson5;

import lesson5.entity.Batman;
import lesson5.entity.Hero;
import lesson5.entity.IronMan;
import lesson5.entity.SpiderMan;

import java.util.Scanner;

public class Game {
    private final Scanner scanner;

    public Game() {
        scanner = new Scanner(System.in);
    }

    public Hero chooseHero() {
        System.out.println("Choose a hero: 1 - Batman, 2 - IronMan, 3 - SpiderMan");
        int heroIndex = scanner.nextInt();
        while (heroIndex > 3 || heroIndex < 1) {
            System.out.println("Your index is not correct. Pls, try again you choice");
            heroIndex = scanner.nextInt();
        }

        switch (heroIndex) {
            case 1:
                return new Batman("BATMAN");
            case 2:
                return new IronMan("IRONMAN");
            case 3:
                return new SpiderMan("SPIDER-MAN");
        }
        return null;
    }

    public void setDataToHero(Hero hero) {
        System.out.println("You hero name is " + hero.getName());
        System.out.println("choose power for your hero");
        final int power = scanner.nextInt();
        System.out.println("choose speed for your hero");
        int speed = scanner.nextInt();
        System.out.println("choose health for your hero");
        double health = scanner.nextDouble();
        hero.setHealth(health);
        hero.setPower(power);
        hero.setSpeed(speed);
    }
}
