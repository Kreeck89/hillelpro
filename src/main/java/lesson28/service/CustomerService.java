package lesson28.service;

import lesson28.Database;
import lesson28.entity.Customer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class CustomerService {
    private String QUERY_SAVE = "INSERT INTO customers (name, surname, email, password, age) VALUES (?,?,?,?,?)";
    private String QUERY_GET_ALL = "SELECT * FROM customers";
    public List<Customer> getAll() {
        List<Customer> customers = new ArrayList<>();

        try (Connection connection = Database.getConnection();
             Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(QUERY_GET_ALL);
            while (resultSet.next()) {
                connection.setAutoCommit(false);
                Customer customer = new Customer();
                customer.setId(resultSet.getInt("id"));
                customer.setName(resultSet.getString("name"));
                customer.setSurname(resultSet.getString("surname"));
                customer.setAge(resultSet.getInt("age"));
                customer.setEmail(resultSet.getString("email"));
                customer.setPassword(resultSet.getString("password"));
                customers.add(customer);
            }
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customers;
    }

    /**
     * @return Пример НЕПРАВИЛЬНОГО использования стейтмента с SQL инъекцией!!!!
     */
    public Customer WRONG_getAll(String name) {
        Customer customer = new Customer();
        try (Connection connection = Database.getConnection();
             Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(
                    "SELECT * FROM customers WHERE name = '" + name + "'");
//            ResultSet resultSet = statement.executeQuery(
//                    "SELECT * FROM customers WHERE name = 'Alex'"); // Этот стейтмент МОЖНО использовать!
            while (resultSet.next()) {
                connection.setAutoCommit(false);
                customer.setId(resultSet.getInt("id"));
                //get all parameters....
            }
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customer;
    }

    public void save(Customer customer) {
        try (Connection connection = Database.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(QUERY_SAVE)) {
            connection.setAutoCommit(false);
            preparedStatement.setString(1, customer.getName());
            preparedStatement.setString(2, customer.getSurname());
            preparedStatement.setString(3, customer.getEmail());
            preparedStatement.setString(4, customer.getPassword());
            preparedStatement.setInt(5, customer.getAge());
            preparedStatement.execute();
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
