package lesson28.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Customer {
    private int id;
    private String name;
    private String surname;
    private int age;
    private String email;
    private String password;
}
