package lesson28;

import lesson28.entity.Customer;
import lesson28.service.CustomerService;

import java.sql.Connection;
import java.util.List;

public class Main {
    public static void main(String[] args) {
//        final Connection connection = Database.getConnection();

        //Statement example:
        final CustomerService customerService = new CustomerService();
//        final List<Customer> customers = customerService.getAll();
//        customers.forEach(System.out::println);

        //PreparedStatement example:
        final Customer customer = new Customer();
        customer.setName("Valentina");
        customer.setSurname("La");
        customer.setAge(50);
        customer.setEmail("valentina_la@mail.ua");
        customer.setPassword("12345qwerty12345");
        customerService.save(customer);
    }
}
