package lesson18;

public class User {
    private String name;

    public User(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
