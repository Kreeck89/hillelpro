package lesson18;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        final Stream<Object> empty = Stream.empty(); // Пусто поток;
//        final Stream<String> stringStream = Stream.of(" ", "first", "second", "third");
        final List<String> strings = Arrays.asList(" ", "first", "second", "third");
        final Stream<String> stringStream = strings.stream();
        final IntStream intStream = IntStream.of(1, 2, 3);

        final Stream<String> sorted = stringStream
                .filter(var -> var.length() > 3)
                .skip(1)
                .distinct()
                .map(element -> {
                    System.out.println("print from stream: " + element);
                    return element + "_mapped";
                })
                .limit(2)
                .sorted();
//                .mapToInt(digit -> Integer.valueOf(digit));

//        final Optional<String> first = sorted.findFirst();
//        System.out.println("first.get(): " + first.get());
//        first = sorted.findFirst(); // Стрим одноразовый!!!! Error: stream has already been operated upon or closed
//        System.out.println("first.get(): " + first.get());

//        final List<String> collect = sorted.collect(Collectors.toList());
//        for (String var : collect) {
//            System.out.println("collect element: " + var);
//        }
//
//        for (String string : strings) {
//            System.out.println("old collection element: " + string);
//        }

        final long count = sorted.count();
        System.out.println("Count of elements: " + count);

        final boolean anyMatch = strings.stream().anyMatch(param -> param.length() < 2);
        System.out.println("anyMatch: " + anyMatch);

        final boolean noneMatch = strings.stream().noneMatch(param -> param.length() < 2);
        System.out.println("nonMatch: " + noneMatch);

        final boolean allMatch = strings.stream().allMatch(param -> param.length() >= 1);
        System.out.println("allMatch: " + allMatch);

        strings.stream()
               .filter(var -> var.length() > 3)
               .skip(1)
               .distinct()
               .map(element -> {
                   System.out.println("print from stream: " + element);
                   return element + "_mapped";
               })
               .limit(2)
               .sorted()
//                .collect(Collectors.toList())
               .forEach(System.out::println);

//        strings.stream().parallel(); //Многопоточная обработка данных!
//        strings.parallelStream();

        strings.stream()
               .mapToInt(Integer::valueOf);

        //Ссылочные методы:
        final Function<String, Integer> function = Integer::parseInt;
        function.apply("1");

        final Integer integer = 5;
        Supplier<String> supplier = integer::toString;
        final String supplierResult = supplier.get();

        final Function<String, String> functionString = String::toUpperCase;
        final String someString = functionString.apply("some string");

        final Function<String, User> userFunction = User::new;
        final User alex = userFunction.apply("Alex");
        System.out.println("User`s name is: " + alex.getName());

        final List<User> collect = strings.stream()
                                          .map(User::new)
                                          .collect(Collectors.toList());

        // Работа с датой:
        final Date date = new Date();
        System.out.println("date is: " + date.getTime());

        final LocalTime localTime = LocalTime.now();
        final LocalDate localDate = LocalDate.now();
        final LocalDateTime localDateTime = LocalDateTime.now();
        System.out.println(localTime);
        System.out.println(localDate);
        System.out.println(localDateTime);

        final LocalTime ofLocalTime = LocalTime.of(10, 15, 55);
        System.out.println(ofLocalTime);

    }

    private Stream<String> methodWithStream(ArrayList<String> strings) {
        if (strings.isEmpty()) {
            return Stream.empty();
        }

//        for (String string : strings) {
//            if (string.length() > 3) {
//                // some logic
//            }
//        }
        return strings.stream();
    }
}
