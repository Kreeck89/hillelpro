package lesson36.service;

import lesson36.entity.Order;
import lesson36.exception.WrongEmailException;

public class OrderService {
    private final EmailService emailService;

    public OrderService(final EmailService emailService) {
        this.emailService = emailService;
    }

    public Order create(final String email, final String name) throws WrongEmailException {
        emailService.validate(email);
        return new Order(1, name, email);
    }
}
