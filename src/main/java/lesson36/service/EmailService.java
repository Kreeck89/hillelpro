package lesson36.service;

import lesson36.exception.WrongEmailException;

public class EmailService {
    public boolean validate(final String email) throws WrongEmailException {
        if (email == null || email.isBlank()) {
            throw new WrongEmailException("email is null or empty");
        }

//        if (email.length() > 3) {
//            return true;
//        }

        return email.contains("@");
    }

    public String format(final String string) {
        return null;
    }
}
