package lesson36;

import lesson36.exception.WrongEmailException;
import lesson36.service.EmailService;

public class Main {
    public static void main(String[] args) throws WrongEmailException {
        final EmailService emailService = new EmailService();
//        emailService.validate(null);
        System.out.println(emailService.validate("alex"));
        System.out.println(emailService.validate("alex@mail.ua"));
    }
}
