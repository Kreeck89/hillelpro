package lesson4.entity;

public class Woman extends Human {

    @Override
    public void work() {
        System.out.println("WOMAN start working...");
        super.work();
    }
}
