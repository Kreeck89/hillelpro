package lesson4.entity;

import lesson4.rule.DefaultRule;

public class Human extends DefaultRule {
    public void work() {
        System.out.println("Default work for HUMAN");
    }

    @Override
    public void printSomething() {
        System.out.println("SOMETHING!!!!");
    }
}
