package lesson4.entity;

import lesson4.rule.RelaxRule;
import lesson4.rule.Rule;
import lesson4.rule.StudyRule;
import lesson4.rule.WorkRule;

public class Fireman extends Man implements Rule {

    @Override
    public void work() {
        System.out.println("Work for STRONG men!!!!");
    }

    @Override
    public void study() {
        System.out.println("Fireman study");
    }

    @Override
    public void relax() {
        System.out.println("Fireman relax");
    }

    @Override
    public void same() {
        System.out.println("same method....");
    }
}
