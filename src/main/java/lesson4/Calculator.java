package lesson4;

public class Calculator {

    //БЕЗ ПОЛИМОРФИЗМА!
//    public int plusInt(int first, int second) {
//        return first + second;
//    }
//
//    public double plusDouble(double first, double second) {
//        return first + second;
//    }
//
//    public long plusLong(long first, long second) {
//        return first + second;
//    }

    //С ПОЛИМОРФИЗМОМ!!!
    public int plus(int first, int second) {
        return first + second;
    }

    public double plus(double first, double second) {
        return first + second;
    }

    public long plus(long first, long second) {
        return first + second;
    }
}
