package lesson4;

import lesson4.entity.Fireman;
import lesson4.entity.Man;
import lesson4.entity.Woman;
import lesson4.rule.DefaultRule;

public class Main {
    public static void main(String[] args) {
        final Calculator calculator = new Calculator();
        double first = 10.0;
        double second = 13.12;
//        calculator.plusInt(first, second); // Без полиморфизма (пример)

        final double plus = calculator.plus(first, second);
        System.out.println("calculator.plus: " + plus);

        final Man man = new Man();
        final Woman woman = new Woman();
        man.work();
        woman.work();
        woman.printConstant();
        woman.printSomething();

        final Fireman fireman = new Fireman();
        fireman.work();
        fireman.study();
        fireman.relax();

//        final Rule rule = new Rule(); //Нельзя создавать интерфейс
//        final DefaultRule defaultRule = new DefaultRule(); // Нельзя создавать абстрактные классы


    }
}
