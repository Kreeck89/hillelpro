package lesson32;

import lesson32.domain.human.Man;
import lesson32.service.ManService;

public class Main {
    public static void main(String[] args) {
        methodWithCtrlAltM();

        final ManService manService = new ManService();
        System.out.println(manService.getById(10));
    }

    private static void methodWithCtrlAltM() {
        final Man man = new Man();
        System.out.println(man);
        System.out.println(man);
        System.out.println(man);
        System.out.println(man);
    }
}
