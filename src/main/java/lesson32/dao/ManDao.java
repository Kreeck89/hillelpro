package lesson32.dao;

import lesson32.domain.human.Gender;
import lesson32.domain.human.Man;

public class ManDao {
    public Man getById(final Integer id) {
        System.out.println("get Man by id emulation....");
        if (id == 1) {
            return new Man(1, Gender.MAN);
        }
        return new Man(id, Gender.WOMAN);
    }
}
