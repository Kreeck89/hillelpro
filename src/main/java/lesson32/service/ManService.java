package lesson32.service;

import lesson32.dao.ManDao;
import lesson32.domain.human.Man;

public class ManService {
    private ManDao manDao = new ManDao();

    /**
     * press alt + enter for javaDoc
     *
     * @param id
     * @return
     */
    public Man getById(final Integer id) {
        System.out.printf("getById. id: %d%n", id); //плохо оставлять комментарии бел ЭКСТРЕННОЙ необходимости!
        final Man manById = manDao.getById(id);
        System.out.printf("getById. man: %d%n", manById.getId());
        return manById;
    }
}
