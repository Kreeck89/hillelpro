package lesson32.domain.human;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class Man extends Human {

    public Man(final Integer id, final Gender gender) {
        super(id, gender);
    }


}
