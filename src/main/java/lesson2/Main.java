package lesson2;

public class Main {

    private static final String SOME_VAR = "some var"; // правильная константа
    private final String SOME_VAR_TWO = "some var";
    public static void main(String[] args) {
        NonPublicVariable nonPublicVariable = new NonPublicVariable();
//        System.out.println(nonPublicVariable.first);

        nonPublicVariable.printFirst();
//        nonPublicVariable.second();
        NonPublicVariable.staticTestCall();

        User user = new User();
//        user.age = 10;
//        user.name = "Alex";
        user.printUserInfo();

        User fullUser = new User("Alex", "Bob", 10);
        fullUser.printUserInfo();

        System.out.println(SOME_VAR);
        System.out.println("Constant.COUNTER: " + Constant.COUNTER);

        final int digital = 10;
//        digital = 15; // не может быть переопределена переменная типа final

        final User finalUser = new User();
//        finalUser = new User(); // не может быть переопределена переменная типа final
//        finalUser.address = "some address"; // не работает для private переменных
        finalUser.setAddress("some address");
        finalUser.setName("Rich");

        System.out.println("finalUser.getAge(): " + finalUser.getAge());
        finalUser.setAge(20);
        System.out.println("finalUser.getAge(): " + finalUser.getAge());

        final Dad dad = new Dad();
        final Son son = new Son();
        son.say();
        dad.say();

    }

    private void testConst() {
        System.out.println(SOME_VAR_TWO);
        System.out.println(SOME_VAR);
    }
}
