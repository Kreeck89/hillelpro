package lesson2;

public class User {
    private String name;
    private String surname;
    private int age;
    private String address;

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(final String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(final int age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(final String address) {
        this.address = address;
    }

    public User() {
    }

    public User(final String name) {
        this.name = name;
    }

//    public User(final String surname) {
//        this.surname = surname;
//    }


    public User(final String name, final int age) {
        this.name = name;
        this.age = age;
    }

    public User(final int age, final String name) {
        this.name = name;
        this.age = age;
    }

    public User(final String name, final String surname, final int age) {
        this.name = name;
        this.surname = surname;
        this.age = age;
    }

    public void printUserInfo() {
        System.out.println("name: " + name);
    }
}
