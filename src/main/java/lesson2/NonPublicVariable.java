package lesson2;

public class NonPublicVariable {
    private int first = 10;

    public void printFirst() {
        System.out.println("first: " + first);
        second();
    }

    private void second() {
        //some logic....
    }

    public static void staticTestCall() {
        System.out.println("staticTestCall!!!!!!!!!!!1");
    }
}
