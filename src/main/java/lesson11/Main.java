package lesson11;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
        String text = "Hello Hillel!\n";
//        write(text);
//        fileWriterWithError();
//        writeWithCreatingFolder(text);
        read();
    }

    private static void fileWriterWithError() throws IOException {
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter("src/main/resources/file.txt", true);
            fileWriter.write("lesson 11\n");
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            fileWriter.close();
            fileWriter.write("unprinted string!");
        }
    }

    private static void write(final String text) /*throws IOException*/ {
        try (final FileWriter fileWriter = new FileWriter("src/main/resources/file.txt", true)) {
            fileWriter.write(text);
        } catch (IOException exception) {
            System.err.println("Error with FileWriter");
        }
//        fileWriter.close(); // close() не нужен, так как есть try с ресурсами = сам закроет поток!
    }

    private static void writeWithCreatingFolder(final String text) throws IOException {
        final String path = createFolderIfNotExist();
        try (final FileWriter fileWriter = new FileWriter(path)) {
            fileWriter.write(text);
        }
    }

    private static String createFolderIfNotExist() throws IOException {
        final String path = "src/main/resources/folder1/folder2/file.txt";
        final Path pathToFile = Paths.get(path);
        if (!Files.exists(pathToFile.getParent())) {
            Files.createDirectories(pathToFile.getParent());
        }
        return path;
    }

    private static void read() throws IOException {
        try (final FileReader fileReader = new FileReader("src/main/resources/file.txt");
             final Scanner scanner = new Scanner(fileReader)) {
            while (scanner.hasNextLine()) {
                System.out.println(scanner.nextLine());
            }
        }
//        scanner.close();
//        fileReader.close();
    }
}
