package lesson11;

import java.util.Scanner;

public class ScannerForPrint {
    public static final Scanner scanner = new Scanner(System.in);

    public static void close() {
        scanner.close();
    }
}
