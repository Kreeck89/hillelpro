package lesson11;

import java.io.FileWriter;
import java.io.IOException;

public class FileUtils {
    public static final String path = "src/main/resources/file.txt";
    final FileWriter fileWriter = new FileWriter(path);

    public FileUtils() throws IOException {
    }

    public void writeInRuntime(final String text) throws IOException {
        fileWriter.write(text);
    }
}
