package lesson19;

import lesson3.enity.Animal;
import lesson3.enity.Cat;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;

public class Main {
    public static void main(String[] args) {
        final List<String> strings = List.of("one", "two", "three");
        final List<Object> objects = List.of("qwerty", 12, new Cat());
        final Set<Integer> integers = Set.of(1, 2, 3);
        final Map<String, String> stringMap = Map.of("keyOne", "valueOne", "keyTwo", "valueTwo");

//        strings.add("four"); // Коллекции созданные с помощью ЛИТЕРАЛОВ НЕИЗМЕНЯЕМЫЕ!

        final Animal cat = new Cat();
//        String name = cat.getName() == null ?: cat.getName();
//        String catName = user ?: user.getCat() ?: user.getCat().getName();

        String elvis = "elvis";
        final String checkedElvis = Objects.requireNonNullElse(elvis, "default elvis");
//        if (elvis == null) {
//            elvis = "default elvis";
//        }
        System.out.println(checkedElvis);

        var integer = 12;
//        integer = "12";

//        ContainerAutoPreferredGapSpring containerAutoPreferredGapSpring = new ContainerAutoPreferredGapSpring();
//        var containerAutoPreferredGapSpring = new ContainerAutoPreferredGapSpring();

        final Function<String, String> stringStringFunction = (var) -> var.concat("prefix");
        System.out.println("lesson19".repeat(10));
        final Optional<String> optional = "optionalString".describeConstable();

        String variable = "one";
        switch (variable) {
            case "one" -> System.out.println("case one");
            case "two" -> System.out.println("case two");
        }

        int digit = 1;
        String result = switch (digit) {
            case 1: yield "one";
            case 2: yield "two";
//            case 3 -> "three";
            default:
                throw new IllegalStateException("Unexpected value: " + digit);
        };

        String text = """
                text block
                next line
                """;
        System.out.println(text);

        final Record connectionPool = new Record("connection pool", 10);
        System.out.println(connectionPool.setting());

//        if (cat instanceof Cat) {
//            ((Cat) cat).getName();
//        }

        if (cat instanceof Cat castedCat) {
            castedCat.getName();
        }
    }
}
