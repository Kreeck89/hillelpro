package lesson36.service;

import lesson36.exception.WrongEmailException;
import lombok.SneakyThrows;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class EmailServiceTest {

    private final EmailService emailService = new EmailService();

    @BeforeEach
    void setUp() {

        System.out.println("run before each tests");
    }

    @BeforeAll
    static void beforeAll() {

        System.out.println("run before ALL tests");
    }

    @AfterEach
    void tearDown() {
        System.out.println("run after each tests");
    }

    @AfterAll
    static void afterAll() {
        System.out.println("run after ALL tests");
    }

    @Test
    void validateIfEmailIsNullTest() {
        final WrongEmailException wrongEmailException = assertThrows(
                WrongEmailException.class,
                () -> emailService.validate(null)
        );

        assertEquals("email is null or empty", wrongEmailException.getMessage());
    }

    @Test
    void validateIfEmailIsEmptyTest() {
        final WrongEmailException wrongEmailException = assertThrows(
                WrongEmailException.class,
                () -> emailService.validate("")
        );

        assertEquals("email is null or empty", wrongEmailException.getMessage());
    }

    @SneakyThrows
    @Test
    void validateIfEmailFormatIsCorrectTest() {
        final String email = "alex@mail.ua";
        final boolean validate = emailService.validate(email);

        assertTrue(validate, String.format("%s is incorrect", email));
    }

    @SneakyThrows
    @Test
    void validateIfEmailFormatIsIncorrectTest() {
        final String email = "alex";
        final boolean validate = emailService.validate(email);

        assertFalse(validate, String.format("%s is correct", email));
    }
}